# go-api-context
Convenience of go context wrapper for innius services.

It provides easy access to innius context values, like company ID, user ID etc. 

![](https://codebuild.us-east-1.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoiTlZmaXlhNVpKWUNqeHh1NFRDL09WdDZOMlZTc09ONXdjRGVpNEFDS3JETmQwS004RTRTQWhuY3ZNd2U3cVYvczI0OUZlQm5qMWJPNGozdmFQdE02bXpJPSIsIml2UGFyYW1ldGVyU3BlYyI6IjV6V0FDWTRkUVlOVkpkMUEiLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)