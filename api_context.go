package apicontext

import (
	"context"
)

// ApiContext
type ApiContext interface {
	Company() string
	CompanyStatus() CompanyStatus
	Domain() string
	UserID() string
	Authenticated() bool
	Subject() string
	Scopes() []string
	context.Context
}

type CompanyStatus int

const (
	CompanyStatusUnknown CompanyStatus = 0
	CompanyStatusTrial                 = 6
	CompanyStatusActive                = 3
	CompanyStatusBlocked               = 4
)

type contextKey string

var companyKey = contextKey("company_id")
var companyStatusKey = contextKey("company_status")
var userIDKey = contextKey("user_id")
var domainKey = contextKey("domain")
var authenticatedKey = contextKey("authenticated")
var subjectKey = contextKey("subject")
var scopesKey = contextKey("scopes")

// returns a new context with the specified company id
func WithCompanyID(c context.Context, companyID string) ApiContext {
	return From(context.WithValue(c, companyKey, companyID))
}

// returns a new context with the specified company_status
func WithCompanyStatus(c context.Context, companyStatus int) ApiContext {
	return From(context.WithValue(c, companyStatusKey, CompanyStatus(companyStatus)))
}

// returns a new context with the specified user id
func WithUserID(c context.Context, userID string) ApiContext {
	return From(context.WithValue(c, userIDKey, userID))
}

// returns a new context with the specified authorization subject
func WithSubject(c context.Context, subject string) ApiContext {
	return From(context.WithValue(c, subjectKey, subject))
}

// returns a new context with the specified domain
func WithDomain(c context.Context, userID string) ApiContext {
	return From(context.WithValue(c, domainKey, userID))
}

// returns a new authenticated context
func Authenticated(c context.Context) ApiContext {
	return From(context.WithValue(c, authenticatedKey, true))
}

// returns a new context with the specified scopes
func WithScopes(c context.Context, scopes []string) ApiContext {
	return From(context.WithValue(c, scopesKey, scopes))
}

// factory method which lifts a standard go context into an ApiContext
func From(c context.Context) ApiContext {
	return &claims{
		Context: c,
	}
}

type claims struct {
	context.Context
}

func (c *claims) Authenticated() bool {
	if s, ok := c.Value(authenticatedKey).(bool); ok {
		return s
	}
	return false
}

func (c *claims) Company() string {
	if s, ok := c.Value(companyKey).(string); ok {
		return s
	}
	return ""
}

func (c *claims) CompanyStatus() CompanyStatus {
	if s, ok := c.Value(companyStatusKey).(CompanyStatus); ok {
		return s
	}
	return CompanyStatusUnknown
}

func (c *claims) Domain() string {
	if s, ok := c.Value(domainKey).(string); ok {
		return s
	}
	return ""
}

func (c *claims) UserID() string {
	if s, ok := c.Value(userIDKey).(string); ok {
		return s
	}
	return ""
}

// returns the authorization subject of the current context.
func (c *claims) Subject() string {
	if s, ok := c.Value(subjectKey).(string); ok {
		return s
	}
	return c.UserID()
}

// returns the scopes of the current context.
func (c *claims) Scopes() []string {
	if s, ok := c.Value(scopesKey).([]string); ok {
		return s
	}
	return nil
}
