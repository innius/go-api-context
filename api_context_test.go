package apicontext

import (
	"context"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestApiContext(t *testing.T) {
	Convey("Api Context", t, func() {
		ctx := From(context.Background())

		Convey("Add company id to the context", func() {
			companyID := "foo"
			ctx2 := WithCompanyID(ctx, companyID)
			Convey("reading the value", func() {
				Convey("should return the company id", func() {
					So(ctx2.Company(), ShouldEqual, companyID)
				})
			})
		})
		Convey("Add company status to the context", func() {
			companyStatus := 3
			ctx2 := WithCompanyStatus(ctx, companyStatus)
			Convey("reading the value", func() {
				Convey("should return the company status", func() {
					So(ctx2.CompanyStatus(), ShouldEqual, companyStatus)
				})
			})
		})
		Convey("Add user id to the context", func() {
			UserID := "user"
			ctx2 := WithUserID(ctx, UserID)
			Convey("reading the value", func() {
				Convey("should return the userid", func() {
					So(ctx2.UserID(), ShouldEqual, UserID)
				})
			})
		})
		Convey("Add domain to the context", func() {
			domain := "user"
			ctx2 := WithDomain(ctx, domain)
			Convey("reading the value", func() {
				Convey("should return the domain", func() {
					So(ctx2.Domain(), ShouldEqual, domain)
				})
			})
		})
		Convey("An authenticated context", func() {
			ctx2 := Authenticated(ctx)
			Convey("reading the value", func() {
				Convey("should be really authenticated", func() {
					So(ctx2.Authenticated(), ShouldEqual, true)
				})
			})
		})
		Convey("with scopes", func() {
			scopes := []string{"read", "write"}
			ctx2 := WithScopes(ctx, scopes)
			Convey("read the value", func() {
				So(ctx2.Scopes(), ShouldResemble, scopes)
			})
		})
		Convey("with subject", func() {
			subject := "subject"
			ctx2 := WithSubject(ctx, subject)
			Convey("read the value", func() {
				So(ctx2.Subject(), ShouldEqual, subject)
			})
		})
		Convey("Chaining context vars", func() {
			cid, uid, domain := "foo", "bar", "baz"
			ctx2 := WithDomain(WithUserID(WithCompanyID(ctx, cid), uid), domain)
			Convey("reading the value", func() {
				Convey("should return the expected context values", func() {
					So(ctx2.Company(), ShouldEqual, cid)
					So(ctx2.UserID(), ShouldEqual, uid)
					So(ctx2.Domain(), ShouldEqual, domain)
				})
			})

		})
	})

}
